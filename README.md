# README #

This README documents whatever steps are necessary to get the application up and running.

### What is this repository for? ###

* Simple anonymous message board server.
* 1.0.0

### How do I get set up? ###

* Requires installation of node.js.
* Once downloaded install the necessary node.js packages by running "npm install"
  on the command line in the root directory of this project.
* To start the server run "node index.js".
* Basic Jest-based tests of the two exposed API methods can be run via "npm run test".

### Who do I talk to? ###

* R.Bellamy


### Further Improvements ###

* Use a schema to validate request JSON.
* Document the API with a package such as Swagger.
* Improve and add to Jest test cases.
* Use of a database instead of local filesystem to persist message board data.
* Add a rate limiter library to prevent spamming of requests if not behind a load balancer.
* Increase level of configuration available via config file.
