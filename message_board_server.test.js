const MessageBoardServer = require('./message_board_server');
const supertest = require('supertest');

let request;
let server;

// Wait for the server to start.
beforeAll(async () => {

    server = new MessageBoardServer();
    await server.init();    
    request = supertest(server.getServer());
});

// Post a message.
it('Posts anonymous messages', async done => {

    const response = await request.post('/')
    .send({ message: 'Hello World!'})
    expect(response.status).toBe(200);
    done();
});

// Get messages.
it('Gets anonymous messages', async done => {

    const response = await request.get('/')
    expect(response.status).toBe(200);
    done();
});

// Finally stop the server.
afterAll(() => {
    server.closeServer();
});
