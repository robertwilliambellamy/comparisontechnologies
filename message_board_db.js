"use strict";

const sqlite3 = require('sqlite3');
const config = require('./config');

class MessageBoardDB {

    // Constructor.
    constructor() {
    }

    // Initialise, connect to DB and create message_board table if it does not exist.
    init() {

        return new Promise((resolve, reject) => {

            this.createDAO();
            this.createTable()
            .then(() => resolve())
            .catch((err) => {
                console.log('Error Initialising DB', err);
                reject(err);
            });
        });
    }

    // Create database access object.
    createDAO() {

        this.db = new sqlite3.Database(config.database, (err) => {

            if (err) {
                console.log('Could not connect to database', err);
            } else {
                console.log('Connected to database');
            }
        });
    }

    // Create message_board table if it does not exist.
    createTable() {

        const sql = `
        CREATE TABLE IF NOT EXISTS message_board (
          id INTEGER PRIMARY KEY AUTOINCREMENT,
          timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
          message TEXT)
        `
        return this.run(sql);
    }

    // Insert a message into the message board.
    insertMessage(message) {

        const sql = `
        INSERT INTO message_board(message) VALUES(?)
        `

        return this.run(sql, [ message ]);
    }

    // Return messages.
    getMessages() {

        const sql = `
        SELECT id, timestamp, message FROM message_board
        `

        return this.all(sql);
    }

    // Run a query against the DB.
    run(sql, params = []) {

        return new Promise((resolve, reject) => {

                this.db.run(sql, params, function (err) {
                if (err) {
                    console.log('Error running sql ' + sql)
                    console.log(err);
                    reject(err);
                }
                else {
                    resolve({ id: this.lastID });
                }
            })
        });
    }

    // Get query from the DB.
    get(sql, params = []) {

        return new Promise((resolve, reject) => {

                this.db.get(sql, params, function (err, result) {
                if (err) {
                    console.log('Error running sql ' + sql)
                    console.log(err);
                    reject(err)
                }
                else {
                    resolve(result);
                }
            })
        });
    }

    // All query from the DB.
    all(sql, params = []) {

        return new Promise((resolve, reject) => {

                this.db.all(sql, params, function (err, rows) {
                if (err) {
                    console.log('Error running sql ' + sql)
                    console.log(err);
                    reject(err)
                }
                else {
                    resolve(rows);
                }
            })
        });
    }
}

module.exports = MessageBoardDB;
