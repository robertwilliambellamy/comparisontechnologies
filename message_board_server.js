"use strict";

const MessageBoardDB = require('./message_board_db');

const fs = require('fs');
const express = require('express');
const bodyParser = require('body-parser');
const config = require('./config');
const app = express();


/**
 * Message board server.
 */
class MessageBoardServer {

    // Constructor.
    constructor() {
    }

    // Wait for DB to initialise before starting server.
    init() {

        return new Promise((resolve, reject) => {

            this.db = new MessageBoardDB();
            this.db.init()
            .then(async () => {                
                await this.createServer();
                resolve();
            })
            .catch((err) => {
                console.log("Error initialising server", err);
                reject(err);
            });
        });
    }

    // Create server.
    createServer() {

        return new Promise((resolve, reject) => {

              // Add 'body-parser' middleware, limit size of message.
              app.use(bodyParser.json({
                  limit: '1mb'
              }));

              // Add message routes.
              app.route('/')
              .get((req, res) => this.getMessages(req, res))
              .post((req, res) => this.postMessage(req, res));

              // Finally listen for requests on the specified port.
              this.server = app.listen(config.port, () => {
                  console.log(`Message Board Server listening at http://localhost:${config.port}`);
                  resolve();
              });
          });
    }

    // Return the Express server instance for use in supertest.
    getServer() {

        return this.server;
    }

    // Close the server.
    closeServer() {

        this.server.close();
    }

    // Handle a get request.
    getMessages(req, res) {

        this.db.getMessages()
        .then((messages) => {
            res.json({
                status: 200,
                messages: messages
            });
        })
        .catch((err) => {
            res.json({
                status: 500
            });
        });
    }

    // Handle a post request.
    postMessage(req, res) {

        // Ensure the post request has message content.
        const body = req.body;
        if (body.message && body.message.length > 0) {

            this.db.insertMessage(body.message)
            .then(() => {
                res.json({
                    status: 200
                });
            })
            .catch((err) => {
                res.json({
                    status: 500
                });
            });
        }
        else {
            res.json({
                status: 500
            });
        }
    }
}

module.exports = MessageBoardServer;
