"use strict";

const MessageBoardServer = require('./message_board_server');
const server = new MessageBoardServer();
server.init();

module.exports = server;
